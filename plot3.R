source('read_data.R')

png('plot3.png', width=480, height=480, units='px', bg = 'transparent')

with(df, {
    plot(datetime, Sub_metering_1, type='l', col='black', ylab='Energy sub metering', xlab='')
    lines(datetime, Sub_metering_2, col='red')
    lines(datetime, Sub_metering_3, col='blue')
    legend(
        'topright',
        legend=c("Sub_metering_1", "Sub_metering_2", "Sub_metering_3"),
        col=c('black', 'red', 'blue'),
        lty=1,
        xjust=1
    )
})

dev.off()
